const init = function init() {
  document.addEventListener('wheel', (event) => {
    const target = event.target;
    if (target.tagName === 'INPUT' && target.type === 'range') {
      event.preventDefault();

      const step = parseFloat(target.attributes.step.value);

      if (event.deltaY < 0) {
        target.value = parseFloat(target.value) + step;
      }

      if (event.deltaY > 0) {
        target.value = parseFloat(target.value) - step;
      }

      if (target.oninput) {
        target.oninput();
      }
      target.dispatchEvent(new Event('input'));
    }
  });
};

module.exports = {
  init,
};
